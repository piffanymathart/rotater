package Rotater;

import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;


class RotaterTest {

	private static final Rotater ROTATER = new Rotater();

	@Test
	void createRotationMatrix_xAxis() {

		Vector3d axis = new Vector3d(1,0,0);
		int denom = 16;
		double[] angles = getAngles(denom);
		Point3d centre = new Point3d(1,2,3);

		Point3d p = new Point3d(0,1,0);
		p.add(centre);

		for(int i=0; i<angles.length; i++) {

			Matrix4d mat = ROTATER.createRotationMatrix(axis, angles[i], centre);

			Point3d actual = new Point3d(p);
			mat.transform(actual);

			Point3d expected = new Point3d(
				0,
				Math.cos(Math.PI*(double)i/(double)denom),
				Math.sin(Math.PI*(double)i/(double)denom)
			);
			expected.add(centre);

			assertApproxEquals(expected, actual);
		}
	}

	@Test
	void createRotationMatrix_yAxis() {

		Vector3d axis = new Vector3d(0,1,0);
		int denom = 16;
		double[] angles = getAngles(denom);
		Point3d centre = new Point3d(1,2,3);

		Point3d p = new Point3d(0,0,1);
		p.add(centre);

		for(int i=0; i<angles.length; i++) {

			Matrix4d mat = ROTATER.createRotationMatrix(axis, angles[i], centre);

			Point3d actual = new Point3d(p);
			mat.transform(actual);

			Point3d expected = new Point3d(
				Math.sin(Math.PI*(double)i/(double)denom),
				0,
				Math.cos(Math.PI*(double)i/(double)denom)
			);
			expected.add(centre);

			assertApproxEquals(expected, actual);
		}
	}

	@Test
	void createRotationMatrix_zAxis() {

		Vector3d axis = new Vector3d(0,0,1);
		int denom = 16;
		double[] angles = getAngles(denom);
		Point3d centre = new Point3d(1,2,3);

		Point3d p = new Point3d(1,0,0);
		p.add(centre);

		for(int i=0; i<angles.length; i++) {

			Matrix4d mat = ROTATER.createRotationMatrix(axis, angles[i], centre);

			Point3d actual = new Point3d(p);
			mat.transform(actual);

			Point3d expected = new Point3d(
				Math.cos(Math.PI*(double)i/(double)denom),
				Math.sin(Math.PI*(double)i/(double)denom),
				0
			);
			expected.add(centre);

			assertApproxEquals(expected, actual);
		}
	}

	@Test
	void createRotationMatrix_diagonalAxis() {

		Vector3d axis = new Vector3d(1,1,1);
		double[] angles = {Math.PI*2.0/3.0, Math.PI*4.0/3.0, Math.PI*6.0/3.0};
		Point3d centre = new Point3d(0,0,0);

		Point3d p = new Point3d(1,0,0);
		p.add(centre);

		for(int i=0; i<3; i++) {

			Matrix4d mat = ROTATER.createRotationMatrix(axis, angles[i], centre);

			Point3d actual = new Point3d(p);
			mat.transform(actual);

			Point3d expected;
			switch(i) {
				case 0: expected = new Point3d(0,1,0); break;
				case 1: expected = new Point3d(0,0,1); break;
				default: expected = new Point3d(1,0,0); break;
			}
			expected.add(centre);

			assertApproxEquals(expected, actual);
		}
	}

	private double[] getAngles(int denom) {
		int n = denom*2;
		double[] angles = new double[n];
		for(int i=0; i<n; i++) {
			angles[i] = 2.0*Math.PI/(double)n*(double)i;
		}
		return angles;
	}

	private void assertApproxEquals(Point3d p, Point3d q) {
		assert(p.distance(q) < 1e-5);
	}
}