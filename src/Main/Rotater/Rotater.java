package Rotater;

import javax.vecmath.*;

/**
 * Manages quaternion rotations.
 */
public class Rotater {

	/**
	 * Creates the rotation matrix for rotating about an axis around a point for some angle.
	 * @param axis The axis about which to rotate.
	 * @param angle The angle to rotate.
	 * @param centre The centre of rotation.
	 * @return The 4x4 affine transformation matrix defining the rotation.
	 */
	public Matrix4d createRotationMatrix(Vector3d axis, double angle, Point3d centre) {
		Quat4d quat = createQuaternion(axis, angle);
		return createRotationMatrix(quat, centre);
	}

	/**
	 * Creates a rotation matrix around a point from a quaternion.
	 * @param q The quaternion.
	 * @param centre The centre of rotation.
	 * @return The 4x4 affine transformation matrix defining the rotation.
	 */
	private Matrix4d createRotationMatrix(Quat4d q, Point3d centre) {

		q.normalize();

		double x = q.x, y = q.y, z = q.z, w = q.w;
		double xx = x * x;
		double xy= x * y;
		double xz = x * z;
		double xw = x * w;

		double yy = y * y;
		double yz = y * z;
		double yw = y * w;

		double zz = z * z;
		double zw = z * w;

		double m00  = 1 - 2 * ( yy + zz );
		double m01  =     2 * ( xy - zw );
		double m02 =     2 * ( xz + yw );

		double m10  =     2 * ( xy + zw );
		double m11  = 1 - 2 * ( xx + zz );
		double m12  =     2 * ( yz - xw );

		double m20  =     2 * ( xz - yw );
		double m21  =     2 * ( yz + xw );
		double m22 = 1 - 2 * ( xx + yy );

		double m03 = 0, m13 = 0, m23 = 0;
		double m30 = 0, m31 = 0, m32 = 0, m33 = 1;

		Matrix4d rotMat = new Matrix4d(m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33);

		Matrix4d preTransMat = getTranslationMatrix(new Vector3d(-centre.x, -centre.y, -centre.z));
		Matrix4d postTransMat = getTranslationMatrix(centre);

		return getProduct(postTransMat, rotMat, preTransMat);
	}

	/**
	 * Creates the quaternion defining rotation about an axis.
	 * @param rotAxis The rotation axis.
	 * @param angle The angle to rotate.
	 * @return The quaternion defining the rotation.
	 */
	private Quat4d createQuaternion(Vector3d rotAxis, double angle) {

		Vector3d axis = new Vector3d(rotAxis);
		axis.normalize();

		double sinVal = halfAngleSine(angle);

		return new Quat4d(
			axis.x * sinVal,
			axis.y * sinVal,
			axis.z * sinVal,
			halfAngleCosine(angle)
		);
	}

	/**
	 * Computes the sine of the half angle.
	 * @param angle The angle.
	 * @return The sine of the half angle.
	 */
	private double halfAngleSine(double angle) {
		double sign = Math.sin(angle/2.0) > 0 ? 1 : -1;
		return Math.abs(Math.sqrt(0.5*(1-Math.cos(angle))))*sign;
	}

	/**
	 * Computes the cosine of the half angle.
	 * @param angle The angle.
	 * @return The cosine of the half angle.
	 */
	private double halfAngleCosine(double angle) {
		double sign = Math.cos(angle/2.0) > 0 ? 1 : -1;
		return Math.abs(Math.sqrt(0.5*(1+Math.cos(angle))))*sign;
	}

	/**
	 * Gets the identity matrix.
	 * @return The 4x4 affine tranformation identity matrix.
	 */
	private Matrix4d getIdentity() {
		Matrix4d identity = new Matrix4d();
		identity.setIdentity();
		return identity;
	}

	/**
	 * Gets the translation matrix defined by the offset.
	 * @param offset The offset by which to translate.
	 * @return The 4x4 affine transformation matrix defining the translation.
	 */
	private Matrix4d getTranslationMatrix(Tuple3d offset) {
		Matrix4d mat = getIdentity();
		mat.setTranslation(new Vector3d(offset));
		return mat;
	}

	/**
	 * Gets the product of three matrices.
	 * @param m1 The first matrix.
	 * @param m2 The second matrix.
	 * @param m3 The third matrix.
	 * @return The matrix product.
	 */
	private Matrix4d getProduct(Matrix4d m1, Matrix4d m2, Matrix4d m3) {
		Matrix4d prod = new Matrix4d(m1);
		prod.mul(m2);
		prod.mul(m3);
		return prod;
	}
}
